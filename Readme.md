#MsmqJavaRepack
Osgi Repack of MsmqJava port for use as osgi bundle in Enonic XP.
Exports packages in ionic.Msmq.*

##Run
"gradlew build"
generates build/libs/MsmqJavaRepack-1.2.1.2.jar

###MsmqJava
Port of MsmqJava to VS2013
https://github.com/ikerlan2015/MsmqJava

------------
###Original Code
https://msmqjava.codeplex.com/

In the releases the dll for 64bit support can be downloaded along with the last version of the library (1.2)

###License
Microsoft Public License (Ms-PL)
